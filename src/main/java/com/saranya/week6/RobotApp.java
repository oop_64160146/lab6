package com.saranya.week6;

public class RobotApp {
    public static void main(String[] args) {
        Robot body = new Robot("Body", 'B', 1, 0);
        Robot petter = new Robot("Petter", 'P', 10, 10);
        body.print();
        body.right();
        body.print();
        body.up();
        body.print();
        body.down();
        body.print();
        body.left();
        body.print();
        body.left();
        body.print();
        body.left();
        body.print();
        petter.print();
        petter.right();
        petter.print();
        petter.up();
        petter.print();
        petter.down();
        petter.print();
        petter.left();
        petter.print();
        petter.left();
        petter.print();
        petter.left();
        petter.print();

        for (int y = Robot.Y_MIN; y <= Robot.Y_MAX; y++) {
            for (int x = Robot.X_MIN; x <= Robot.X_MAX; x++) {
                if (body.getX() == x && body.getY() == y) {
                    System.out.print(body.getSymblo());
                } else if (petter.getX() == x && petter.getY() == y) {
                    System.out.print(petter.getSymblo());
                } else {
                    System.out.print("-");
                }

            }
            System.out.println();
        }
    }
}
